package com.example.eheca.mp3metadataremote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import wseemann.media.FFmpegMediaMetadataRetriever;

public class MainActivity extends AppCompatActivity {
    private ImageView artImageView;
    private TextView urlTextView, titleTextView, albumTextView, artistTextView;
    private EditText urlEditText;
    private LinearLayout datosLayout;
    private Button consultarButton;
    private ProgressBar progressBar;
    //private final String songUrl = "https://www.dropbox.com/s/1wnvn8eianep5wy/3.%20The%20Boy%20Who%20Destroyed%20The%20World.mp3?dl=1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.artImageView = findViewById(R.id.artImageView);

        this.urlTextView = findViewById(R.id.urlTextView);
        this.titleTextView = findViewById(R.id.titleTextView);
        this.artistTextView =  findViewById(R.id.artistTextView);
        this.albumTextView = findViewById(R.id.albumTextView);
        this.urlEditText = findViewById(R.id.urlEditText);
        this.datosLayout = findViewById(R.id.datosLayout);
        this.progressBar = findViewById(R.id.progressBar);
        this.consultarButton = findViewById(R.id.consultarButton);

    }
    public void obtener(View view){
        String songUrl = urlEditText.getText().toString();
        if (!songUrl.isEmpty()){
            urlTextView.setText(songUrl);

            datosLayout.setVisibility(View.GONE);
            consultarButton.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(songUrl, new HashMap<String, String>());


            String title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            String album = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            String artist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            byte[] artData = retriever.getEmbeddedPicture();
            Bitmap art = BitmapFactory.decodeByteArray(artData, 0, artData.length);

            titleTextView.setText(title != null ? title : "Null");
            albumTextView.setText(album != null ? album : "Null");
            artistTextView.setText(artist != null ? artist : "Null");
            if (art != null)
                artImageView.setImageBitmap(art);

            datosLayout.setVisibility(View.VISIBLE);
            consultarButton.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }else {
            urlEditText.setError("No puede estar vacío");
        }
    }
}
